############# HOTELS - Hotels Along the Croatian Coast
# source: https://www.spoj.com/problems/HOTELS/

count_of_hotels, budget = gets.split(' ').map(&:to_i)
hotels_prices = gets.split(' ').map(&:to_i)
sum = 0
window_start = 0
total = 0

count_of_hotels.times do |window_end|
  sum += hotels_prices[window_end]
  while sum > budget
    sum -= hotels_prices[window_start]
    window_start += 1
  end
  total = [sum, total].max
end


print total