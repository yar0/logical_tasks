##ONP - Transform the Expression
Transform the algebraic expression with brackets into RPN form (Reverse Polish Notation). Two-argument operators: +, -, *, /, ^ (priority from the lowest to the highest), brackets ( ). Operands: only letters: a,b,...,z. Assume that there is only one RPN form (no expressions like a*b*c).

#### Input

```
t [the number of expressions <= 100]
expression [length <= 400]
[other expressions]
```
####Example
```
Input:
3
(a+(b*c))
((a+b)*(z+x))
((a+t)*((b+(a+c))^(c+d)))

Output:
abc*+
ab+zx+*
at+bac++cd+^*
```
[реалізація](https://bitbucket.org/yar0/logical_tasks/src/master/algebraic_expression_into_rpn.rb)

##HOTELS - Hotels Along the Croatian Coast

There are N hotels along the beautiful Adriatic coast. Each hotel has its value in Euros.

Sroljo has won M Euros on the lottery. Now he wants to buy a sequence of consecutive hotels, such that the sum of the values of these consecutive hotels is as great as possible - but not greater than M.

You are to calculate this greatest possible total value.

####Input
In the first line of the input there are integers N and M (1 ≤ N ≤ 300 000, 1 ≤ M < 231).

In the next line there are N natural numbers less than 106, representing the hotel values in the order they lie along the coast.
####Output
Print the required number (it will be greater than 0 in all of the test data).

####EXAMPLE
```
Input                                   Input
5 12                                    4 9
2 1 3 4 5                               7 3 5 6
        
Output                                  Output
12                                      8

```
[реалізація](https://bitbucket.org/yar0/logical_tasks/src/master/hotels_problem.rb)
