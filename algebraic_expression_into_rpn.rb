######### ONP - Transform the Expression
# source: https://www.spoj.com/problems/ONP/

def operation?(operation)
  operations = %w[+ - / * ^]
  operations.include?(operation)
end

def handle_operation(operation, op_stack, out)
  if op_stack.empty?
    op_stack.push(operation)
  elsif op_stack.last != '(' && compare_priority(op_stack.last, operation) > 0
    out.push(op_stack.pop)
    op_stack.push(operation)
  else
    op_stack.push(operation)
  end
end

def handle_bracket(bracket, op_stack, out)
  if bracket == '('
    op_stack.push(bracket)
  else
    operation = nil
    while operation != '('
      operation = op_stack.pop
      out.push(operation) unless operation == '(' # TODO: refactor, check
    end
  end
end

def handle_operand(operand, out)
  out.push(operand)
end

def compare_priority(op1, op2)
  priorities = { '+' => 0, '-' => 0, '*' => 1, '/' => 1, '^' => 2 }
  priorities[op1] <=> priorities[op2]
end

def operand?(char)
  ('a'..'z').cover?(char)
end

def bracket?(char)
  %w[( )].include?(char)
end

def transform(expression)
  out = []
  operations_stack = []
  expression.split('').each do |char|
    if operand?(char)
      handle_operand(char, out)
    elsif bracket?(char)
      handle_bracket(char, operations_stack, out)
    elsif operation?(char)
      handle_operation(char, operations_stack, out)
    end
  end
  out.join('') + operations_stack.join('')
end

n = gets.to_i
expressions = []
n.times do
  expressions << gets.chomp
end

expressions.each do |exp|
  $stdout.puts transform(exp)
end
